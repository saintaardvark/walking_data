# Original source:  https://github.com/twstokes/healthdata_influx
# Original license:
# MIT License

# Copyright (c) 2021 Tanner Stokes

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Thanks very much for the software, Tanner!

"""
Parses an Apple Health export file
and imports into InfluxDB
"""
import sys
import argparse
from datetime import datetime

from loguru import logger
from lxml import etree

from db import InfluxDBUploader


class Importer:
    """
    Importer parses an Apple Health XML file
    and uploads Records to a database
    """

    def __init__(self, uploader, dry=False, buffer_size=50000):
        if dry:
            logger.info("Dry run - no database changes will be made.")

        self.dry = dry
        self.uploader = uploader
        # how many records to Buffer before flushing to the database
        # adjusting this will affect memory consumption
        self.buffer_size = buffer_size

    def upload(self, points):
        """
        Sends points to the uploader if not a dry run.
        """
        if not self.dry:
            self.uploader.upload(points)

    def parse_and_upload(self, export_path):
        """
        Takes InfluxDB configuration and Apple Health Data file paths
        Uploads to InfluxDB
        """

        def create_flusher(buffer, size):
            def flusher(records):
                logger.info(
                    "Flushing {} points to DB. Current total: {}".format(size, records)
                )
                self.upload(buffer[:size])
                # clean up
                del buffer[:size]

            return flusher

        try:
            logger.info("Opening export file...")
            with open(export_path, mode="rb") as file:
                context = self.get_record_iterator(file)

                point_buffer = []
                total_records, success_records = (0, 0)
                flusher = create_flusher(point_buffer, self.buffer_size)

                for idx, (_, record) in enumerate(context):
                    total_records += 1

                    try:
                        point = self.mung_record_to_point(record)
                        point_buffer.append(point)
                        success_records += 1
                    except Exception as error:
                        output_mung_error(error, record, idx + 1)

                    if len(point_buffer) > self.buffer_size - 1:
                        flusher(total_records)

                    # memory cleanup
                    record.clear()
                    while record.getprevious() is not None:
                        del record.getparent()[0]

                # upload the rest
                self.upload(point_buffer)

            logger.info("Successful uploads: {}".format(success_records))
            logger.info("Total records: {}".format(total_records))
        except Exception as error:
            logger.info("Failure!")
            logger.info(sys.exc_info())
            logger.info(error)

    def mung_record_to_point(self, record):
        """
        Returns an InfluxDB point for a health record XML element
        """
        attr = record.attrib

        if "endDate" not in attr or "value" not in attr or "type" not in attr:
            raise ValueError("Failed to find all required fields.")

        tags = {}
        fields = {}

        value = attr["value"]
        end_date = attr["endDate"]
        measurement = attr["type"]

        try:
            # try to convert to a number
            value = float(value)
        except ValueError:
            # carry on as a string
            pass

        # set the fields
        fields["value"] = value
        # convert to datetime obj
        time = datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S %z")

        if "unit" in attr:
            tags["unit"] = attr["unit"]
        if "sourceName" in attr:
            tags["source"] = attr["sourceName"]

        point = self.uploader.create_point(measurement, time, fields, tags)

        return point

    def get_record_iterator(self, file):
        """
        Takes in an Apple Health Data
        export file, returns iterator for Record elements
        """
        return etree.iterparse(file, events=("end",), tag="Record")


def output_mung_error(error, record, index):
    logger.info("Couldn't convert record to point:", error)
    logger.info(etree.tostring(record))
    logger.info("Record index: {}".format(index))


def main():
    """
    Main entry point
    """
    try:
        UPLOADER = InfluxDBUploader(ARGS.config_path)
        logger.info("InfluxDB uploader loaded.")
        IMPORTER = Importer(UPLOADER, ARGS.dry)
        logger.info("Importer loaded.")
        IMPORTER.parse_and_upload(ARGS.export_path)
    except FileNotFoundError:
        logger.warning("Could not load InfluxDB configuration file!")
    except Exception as error:
        logger.warning("Failed to initialize InfluxDB!")
        logger.warning(error)


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(
        description="Imports Apple Health Data to InfluxDB"
    )

    PARSER.add_argument(
        "--config_path", help="InfluxDB config file path", default="./config.yml"
    )
    PARSER.add_argument(
        "--dry", help="Dry run - no DB changes", action="store_true", default=False
    )
    PARSER.add_argument("export_path", help="Apple Health Data export file path")

    ARGS = PARSER.parse_args()

    main()
