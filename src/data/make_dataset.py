#!/usr/bin/env python3

import subprocess

import click
from loguru import logger



@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger.info("making final data set from raw data")
    subprocess.run(["unzip", "-o", "-d", output_filepath, input_filepath])
    logger.info("Done!")


if __name__ == "__main__":
    main()
